import 'dart:async';

import 'package:flutter/material.dart';
import 'package:horse_run/layout/main_layout.dart';
import 'package:horse_run/widgets/horse.dart';

class DrawMap extends StatefulWidget {
  @override
  _DrawMapState createState() => _DrawMapState();
}

class _DrawMapState extends State<DrawMap> {

  Timer _timer2;
  final timeout2 = const Duration(milliseconds: 10);
  double radiusCurrent = 10;

  startTimeout2() {
    _timer2 = new Timer.periodic(timeout2, (Timer timer) {
      setState(() {

      });
    });

  }
  @override
  void initState() {
    startTimeout2();

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final phoneSize = MediaQuery.of(context);
    return MainLayout(
      Horse(phoneSize.size.width),
    );
  }
}
