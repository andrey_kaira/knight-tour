import 'package:flutter/material.dart';

class MainLayout extends StatefulWidget {
  final Widget horse;

  MainLayout(
    this.horse,
  );

  @override
  _MainLayoutState createState() => _MainLayoutState();
}

class _MainLayoutState extends State<MainLayout> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Horse run'),
      ),
      body: Stack(
        children: <Widget>[
          widget.horse,
        ],
      ),
    );
  }
}
