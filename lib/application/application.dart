import 'package:flutter/material.dart';
import 'package:horse_run/screen/draw_map.dart';

class Application extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: DrawMap(),
    );
  }
}
