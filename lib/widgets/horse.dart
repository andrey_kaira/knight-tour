import 'dart:async';

import 'package:flutter/material.dart';
import 'package:horse_run/providers/horse_settings.dart';
import 'package:provider/provider.dart';

class Horse extends StatefulWidget {
  double size;

  Horse(this.size);

  @override
  _HorseState createState() => _HorseState();
}

class _HorseState extends State<Horse> {
  Timer timer;
  final timeout = Duration(milliseconds: 10);
  double radiusCurrent = 10;

  List<int> matrix = new List<int>();

  double targetX = -1;
  double targetY = -1;

  double dx = 108;
  double dy = 54;

  int speed = 2;

  Wrap wrap = Wrap(
    children: <Widget>[],
  );

  bool stepLeftDown(double X, double Y, int size, int i, int j) {
    if (X + i < size &&
        Y + j < size &&
        matrix[X.toInt() + (Y.toInt() * size.toInt())] == matrix[(X.toInt() + i + ((Y.toInt() * size.toInt()) + (j * size.toInt())))] &&
        matrix[X.toInt() + i + ((Y.toInt() * size.toInt()) + (j * size.toInt()))] != 0) {
      targetX = X + i;
      targetY = Y + j;
      return true;
    }
    return false;
  }

  bool stepRightDown(double X, double Y, int size, int i, int j) {
    if (X - i >= 0 &&
        Y + j < size &&
        Y + j < size &&
        matrix[X.toInt() + (Y.toInt() * size.toInt())] == matrix[X.toInt() - i + ((Y.toInt() * size.toInt()) + (j * size.toInt()))] &&
        matrix[X.toInt() - i + ((Y.toInt() * size.toInt()) + (j * size.toInt()))] != 0) {
      targetX = X - i;
      targetY = Y + j;
      return true;
    }
    return false;
  }

  bool stepLeftUp(double X, double Y, int size, int i, int j) {
    if (X + i < size &&
        Y - j >= 0 &&
        matrix[X.toInt() + (Y.toInt() * size.toInt())] == matrix[(X.toInt() + i) + ((Y.toInt() * size.toInt()) - (j * size.toInt()))] &&
        matrix[X.toInt() + i + ((Y.toInt() * size.toInt()) - (j * size.toInt()))] != 0) {
      targetX = X + i;
      targetY = Y - j;
      return true;
    }
    return false;
  }

  bool stepRightUp(double X, double Y, int size, int i, int j) {
    if (X - i >= 0 &&
        Y - j >= 0 &&
        matrix[X.toInt() + (Y.toInt() * size.toInt())] == matrix[X.toInt() - i + ((Y.toInt() * size.toInt()) - (j * size.toInt()))] &&
        matrix[X.toInt() - i + ((Y.toInt() * size.toInt()) - (j * size.toInt()))] != 0) {
      targetX = X - i;
      targetY = Y - j;
      return true;
    }
    return false;
  }

  int count = 0;

  startTimeout() {
    timer = new Timer.periodic(timeout, (Timer timer) {
      setState(() {
        double X = (dx / Provider.of<HorseSetting>(context).sizeField);
        double Y = (dy / Provider.of<HorseSetting>(context).sizeField);
        int size = HorseSetting.size;
        if (targetX == -1) {
          count++;
          Provider.of<HorseSetting>(context).setblockStop(true);
          initWarp();
          //wrap = Wrap(children: wrap.children.where((element) => element!=wrap.children));
          // matrix[(X.toInt() + (size * Y).toInt())] = 0;
          if (X >= size / 4 && X <= size - (size / 4) && Y - 1 > size / 4 && Y < size - (size / 4)) {
            if (stepLeftDown(X, Y, size, 1, 2))
              ;
            else if (stepRightDown(X, Y, size, 1, 2))
              ;
            else if (stepLeftUp(X, Y, size, 1, 2))
              ;
            else if (stepRightUp(X, Y, size, 1, 2))
              ;
            else if (stepRightDown(X, Y, size, 2, 1))
              ;
            else if (stepRightUp(X, Y, size, 2, 1))
              ;
            else if (stepLeftUp(X, Y, size, 2, 1))
              ;
            else if (stepLeftDown(X, Y, size, 2, 1))
              ;
            else {
              if (X - 1 >= 0 &&
                  Y + 2 < size &&
                  matrix[X.toInt() - 1 + ((Y.toInt() * size.toInt()) + (2 * size.toInt()))] >= 1 &&
                  matrix[X.toInt() - 1 + ((Y.toInt() * size.toInt()) + (2 * size.toInt()))] <= 4 &&
                  matrix[X.toInt() - 1 + ((Y.toInt() * size.toInt()) + (2 * size.toInt()))] != 0) {
                targetX = X - 1;
                targetY = Y + 2;
              } else if (X + 1 < size &&
                  Y + 2 < size &&
                  matrix[X.toInt() + 1 + ((Y.toInt() * size.toInt()) + (2 * size.toInt()))] >= 1 &&
                  matrix[X.toInt() + 1 + ((Y.toInt() * size.toInt()) + (2 * size.toInt()))] <= 4 &&
                  matrix[X.toInt() + 1 + ((Y.toInt() * size.toInt()) + (2 * size.toInt()))] != 0) {
                targetX = X + 1;
                targetY = Y + 2;
              } else if (X - 1 >= 0 &&
                  Y - 2 >= 0 &&
                  Y - 2 < size &&
                  matrix[X.toInt() - 1 + ((Y.toInt() * size.toInt()) - (2 * size.toInt()))] >= 1 &&
                  matrix[X.toInt() - 1 + ((Y.toInt() * size.toInt()) - (2 * size.toInt()))] <= 4 &&
                  matrix[X.toInt() - 1 + ((Y.toInt() * size.toInt()) - (2 * size.toInt()))] != 0) {
                targetX = X - 1;
                targetY = Y - 2;
              } else if (X + 1 < size &&
                  Y - 2 >= 0 &&
                  Y - 2 < size &&
                  matrix[X.toInt() + 1 + ((Y.toInt() * size.toInt()) - (2 * size.toInt()))] >= 1 &&
                  matrix[X.toInt() + 1 + ((Y.toInt() * size.toInt()) - (2 * size.toInt()))] <= 4 &&
                  matrix[X.toInt() + 1 + ((Y.toInt() * size.toInt()) - (2 * size.toInt()))] != 0) {
                targetX = X + 1;
                targetY = Y - 2;
              } else if (X - 2 >= 0 &&
                  Y + 1 < size &&
                  Y + 1 < size &&
                  matrix[X.toInt() - 2 + ((Y.toInt() * size.toInt()) + (1 * size.toInt()))] >= 1 &&
                  matrix[X.toInt() - 2 + ((Y.toInt() * size.toInt()) + (1 * size.toInt()))] <= 4 &&
                  matrix[X.toInt() - 2 + ((Y.toInt() * size.toInt()) + (1 * size.toInt()))] != 0) {
                targetX = X - 2;
                targetY = Y + 1;
              } else if (X + 2 < size &&
                  Y + 1 < size &&
                  Y + 1 < size &&
                  matrix[X.toInt() + 2 + ((Y.toInt() * size.toInt()) + (1 * size.toInt()))] >= 1 &&
                  matrix[X.toInt() + 2 + ((Y.toInt() * size.toInt()) + (1 * size.toInt()))] <= 4 &&
                  matrix[X.toInt() + 2 + ((Y.toInt() * size.toInt()) + (1 * size.toInt()))] != 0) {
                targetX = X + 2;
                targetY = Y + 1;
              } else if (X - 2 >= 0 &&
                  Y - 1 >= 0 &&
                  matrix[X.toInt() - 2 + ((Y.toInt() * size.toInt()) - (1 * size.toInt()))] >= 1 &&
                  matrix[X.toInt() - 2 + ((Y.toInt() * size.toInt()) - (1 * size.toInt()))] <= 4 &&
                  matrix[X.toInt() - 2 + ((Y.toInt() * size.toInt()) - (1 * size.toInt()))] != 0) {
                targetX = X - 2;
                targetY = Y - 1;
              } else if (X + 2 < size &&
                  Y - 1 >= 0 &&
                  matrix[X.toInt() + 2 + ((Y.toInt() * size.toInt()) - (1 * size.toInt()))] >= 1 &&
                  matrix[X.toInt() + 2 + ((Y.toInt() * size.toInt()) - (1 * size.toInt()))] <= 4 &&
                  matrix[X.toInt() + 2 + ((Y.toInt() * size.toInt()) - (1 * size.toInt()))] != 0) {
                targetX = X + 2;
                targetY = Y - 1;
              } else if (X - 1 >= 0 &&
                  Y + 2 < size &&
                  Y + 2 < size &&
                  matrix[X.toInt() - 1 + ((Y.toInt() * size.toInt()) + (2 * size.toInt()))] > 4 &&
                  matrix[X.toInt() - 1 + ((Y.toInt() * size.toInt()) + (2 * size.toInt()))] <= 8 &&
                  matrix[X.toInt() - 1 + ((Y.toInt() * size.toInt()) + (2 * size.toInt()))] != 0) {
                targetX = X - 1;
                targetY = Y + 2;
              } else if (X + 1 < size &&
                  Y + 2 < size &&
                  Y + 2 < size &&
                  matrix[X.toInt() + 1 + ((Y.toInt() * size.toInt()) + (2 * size.toInt()))] > 4 &&
                  matrix[X.toInt() + 1 + ((Y.toInt() * size.toInt()) + (2 * size.toInt()))] <= 8 &&
                  matrix[X.toInt() + 1 + ((Y.toInt() * size.toInt()) + (2 * size.toInt()))] != 0) {
                targetX = X + 1;
                targetY = Y + 2;
              } else if (X - 1 >= 0 &&
                  Y - 2 >= 0 &&
                  Y - 2 < size &&
                  matrix[X.toInt() - 1 + ((Y.toInt() * size.toInt()) - (2 * size.toInt()))] > 4 &&
                  matrix[X.toInt() - 1 + ((Y.toInt() * size.toInt()) - (2 * size.toInt()))] <= 8 &&
                  matrix[X.toInt() - 1 + ((Y.toInt() * size.toInt()) - (2 * size.toInt()))] != 0) {
                targetX = X - 1;
                targetY = Y - 2;
              } else if (X + 1 < size &&
                  Y - 2 >= 0 &&
                  Y - 2 < size &&
                  matrix[X.toInt() + 1 + ((Y.toInt() * size.toInt()) - (2 * size.toInt()))] > 4 &&
                  matrix[X.toInt() + 1 + ((Y.toInt() * size.toInt()) - (2 * size.toInt()))] <= 8 &&
                  matrix[X.toInt() - 1 + ((Y.toInt() * size.toInt()) - (2 * size.toInt()))] != 0) {
                targetX = X + 1;
                targetY = Y - 2;
              } else if (X - 2 >= 0 &&
                  Y + 1 < size &&
                  Y + 1 < size &&
                  matrix[X.toInt() - 2 + ((Y.toInt() * size.toInt()) + (1 * size.toInt()))] > 4 &&
                  matrix[X.toInt() - 2 + ((Y.toInt() * size.toInt()) + (1 * size.toInt()))] <= 8 &&
                  matrix[X.toInt() - 2 + ((Y.toInt() * size.toInt()) + (1 * size.toInt()))] != 0) {
                targetX = X - 2;
                targetY = Y + 1;
              } else if (X + 2 < size &&
                  Y + 1 < size &&
                  Y + 1 < size &&
                  matrix[X.toInt() + 2 + ((Y.toInt() * size.toInt()) + (1 * size.toInt()))] > 4 &&
                  matrix[X.toInt() + 2 + ((Y.toInt() * size.toInt()) + (1 * size.toInt()))] <= 8 &&
                  matrix[X.toInt() + 2 + ((Y.toInt() * size.toInt()) + (1 * size.toInt()))] != 0) {
                targetX = X + 2;
                targetY = Y + 1;
              } else if (X - 2 >= 0 &&
                  Y - 1 >= 0 &&
                  Y - 1 < size &&
                  matrix[X.toInt() - 2 + ((Y.toInt() * size.toInt()) - (1 * size.toInt()))] > 4 &&
                  matrix[X.toInt() - 2 + ((Y.toInt() * size.toInt()) - (1 * size.toInt()))] <= 8 &&
                  matrix[X.toInt() - 2 + ((Y.toInt() * size.toInt()) - (1 * size.toInt()))] != 0) {
                targetX = X - 2;
                targetY = Y - 1;
              } else if (X + 2 < size &&
                  Y - 1 >= 0 &&
                  Y - 1 < size &&
                  matrix[X.toInt() + 2 + ((Y.toInt() * size.toInt()) - (1 * size.toInt()))] > 4 &&
                  matrix[X.toInt() + 2 + ((Y.toInt() * size.toInt()) - (1 * size.toInt()))] <= 8 &&
                  matrix[X.toInt() - 2 + ((Y.toInt() * size.toInt()) - (1 * size.toInt()))] != 0) {
                targetX = X + 2;
                targetY = Y - 1;
              } else {
                timer.cancel();
                bool victory = true;
                matrix.forEach((item) {
                  if (item != 0) victory = false;
                  if (victory == true)
                    print("Конь закончил прогулку!");
                  else
                    print("Конь заблудился! ;(");
                });
              }
            }
          } else {
            if (stepLeftDown(X, Y, size, 1, 2))
              ;
            else if (stepRightDown(X, Y, size, 1, 2))
              ;
            else if (stepLeftUp(X, Y, size, 1, 2))
              ;
            else if (stepRightUp(X, Y, size, 1, 2))
              ;
            else if (stepRightDown(X, Y, size, 2, 1))
              ;
            else if (stepRightUp(X, Y, size, 2, 1))
              ;
            else if (stepLeftUp(X, Y, size, 2, 1))
              ;
            else if (stepLeftDown(X, Y, size, 2, 1))
              ;
            else {
              if (X - 1 >= 0 &&
                  Y + 2 < size &&
                  Y + 2 < size &&
                  matrix[X.toInt() - 1 + ((Y.toInt() * size.toInt()) + (2 * size.toInt()))] > 4 &&
                  matrix[X.toInt() - 1 + ((Y.toInt() * size.toInt()) + (2 * size.toInt()))] <= 8 &&
                  matrix[X.toInt() - 1 + ((Y.toInt() * size.toInt()) + (2 * size.toInt()))] != 0) {
                targetX = X - 1;
                targetY = Y + 2;
              } else if (X + 1 < size &&
                  Y + 2 < size &&
                  Y + 2 < size &&
                  matrix[X.toInt() + 1 + ((Y.toInt() * size.toInt()) + (2 * size.toInt()))] > 4 &&
                  matrix[X.toInt() + 1 + ((Y.toInt() * size.toInt()) + (2 * size.toInt()))] <= 8 &&
                  matrix[X.toInt() + 1 + ((Y.toInt() * size.toInt()) + (2 * size.toInt()))] != 0) {
                targetX = X + 1;
                targetY = Y + 2;
              } else if (X - 1 >= 0 &&
                  Y - 2 >= 0 &&
                  Y - 2 < size &&
                  matrix[X.toInt() - 1 + ((Y.toInt() * size.toInt()) - (2 * size.toInt()))] > 4 &&
                  matrix[X.toInt() - 1 + ((Y.toInt() * size.toInt()) - (2 * size.toInt()))] <= 8 &&
                  matrix[X.toInt() - 1 + ((Y.toInt() * size.toInt()) - (2 * size.toInt()))] != 0) {
                targetX = X - 1;
                targetY = Y - 2;
              } else if (X + 1 < size &&
                  Y - 2 >= 0 &&
                  matrix[X.toInt() + 1 + ((Y.toInt() * size.toInt()) - (2 * size.toInt()))] > 4 &&
                  matrix[X.toInt() + 1 + ((Y.toInt() * size.toInt()) - (2 * size.toInt()))] <= 8 &&
                  matrix[X.toInt() + 1 + ((Y.toInt() * size.toInt()) - (2 * size.toInt()))] != 0) {
                targetX = X + 1;
                targetY = Y - 2;
              } else if (X - 2 >= 0 &&
                  Y + 1 < size &&
                  matrix[X.toInt() - 2 + ((Y.toInt() * size.toInt()) + (1 * size.toInt()))] > 4 &&
                  matrix[X.toInt() - 2 + ((Y.toInt() * size.toInt()) + (1 * size.toInt()))] <= 8 &&
                  matrix[X.toInt() - 2 + ((Y.toInt() * size.toInt()) + (1 * size.toInt()))] != 0) {
                targetX = X - 2;
                targetY = Y + 1;
              } else if (X + 2 < size &&
                  Y + 1 < size &&
                  matrix[X.toInt() + 2 + ((Y.toInt() * size.toInt()) + (1 * size.toInt()))] > 4 &&
                  matrix[X.toInt() + 2 + ((Y.toInt() * size.toInt()) + (1 * size.toInt()))] <= 8 &&
                  matrix[X.toInt() + 2 + ((Y.toInt() * size.toInt()) + (1 * size.toInt()))] != 0) {
                targetX = X + 2;
                targetY = Y + 1;
              } else if (X - 2 >= 0 &&
                  Y - 1 >= 0 &&
                  Y - 1 < size &&
                  matrix[X.toInt() - 2 + ((Y.toInt() * size.toInt()) - (1 * size.toInt()))] > 4 &&
                  matrix[X.toInt() - 2 + ((Y.toInt() * size.toInt()) - (1 * size.toInt()))] <= 8 &&
                  matrix[X.toInt() - 2 + ((Y.toInt() * size.toInt()) - (1 * size.toInt()))] != 0) {
                targetX = X - 2;
                targetY = Y - 1;
              } else if (X + 2 < size &&
                  Y - 1 >= 0 &&
                  matrix[X.toInt() + 2 + ((Y.toInt() * size.toInt()) - (1 * size.toInt()))] > 4 &&
                  matrix[X.toInt() + 2 + ((Y.toInt() * size.toInt()) - (1 * size.toInt()))] <= 8 &&
                  matrix[X.toInt() - 2 + ((Y.toInt() * size.toInt()) - (1 * size.toInt()))] != 0) {
                targetX = X + 2;
                targetY = Y - 1;
              } else if (X - 1 >= 0 &&
                  Y + 2 < size &&
                  matrix[X.toInt() - 1 + ((Y.toInt() * size.toInt()) + (2 * size.toInt()))] >= 1 &&
                  matrix[X.toInt() - 1 + ((Y.toInt() * size.toInt()) + (2 * size.toInt()))] <= 4 &&
                  matrix[X.toInt() - 1 + ((Y.toInt() * size.toInt()) + (2 * size.toInt()))] != 0) {
                targetX = X - 1;
                targetY = Y + 2;
              } else if (X + 1 < size &&
                  Y + 2 < size &&
                  matrix[X.toInt() + 1 + ((Y.toInt() * size.toInt()) + (2 * size.toInt()))] >= 1 &&
                  matrix[X.toInt() + 1 + ((Y.toInt() * size.toInt()) + (2 * size.toInt()))] <= 4 &&
                  matrix[X.toInt() + 1 + ((Y.toInt() * size.toInt()) + (2 * size.toInt()))] != 0) {
                targetX = X + 1;
                targetY = Y + 2;
              } else if (X - 1 >= 0 &&
                  Y - 2 >= 0 &&
                  matrix[X.toInt() - 1 + ((Y.toInt() * size.toInt()) - (2 * size.toInt()))] >= 1 &&
                  matrix[X.toInt() - 1 + ((Y.toInt() * size.toInt()) - (2 * size.toInt()))] <= 4 &&
                  matrix[X.toInt() - 1 + ((Y.toInt() * size.toInt()) - (2 * size.toInt()))] != 0) {
                targetX = X - 1;
                targetY = Y - 2;
              } else if (X + 1 < size &&
                  Y - 2 >= 0 &&
                  matrix[X.toInt() + 1 + ((Y.toInt() * size.toInt()) - (2 * size.toInt()))] >= 1 &&
                  matrix[X.toInt() + 1 + ((Y.toInt() * size.toInt()) - (2 * size.toInt()))] <= 4 &&
                  matrix[X.toInt() + 1 + ((Y.toInt() * size.toInt()) - (2 * size.toInt()))] != 0) {
                targetX = X + 1;
                targetY = Y - 2;
              } else if (X - 2 >= 0 &&
                  Y + 1 < size &&
                  matrix[X.toInt() - 2 + ((Y.toInt() * size.toInt()) + (1 * size.toInt()))] >= 1 &&
                  matrix[X.toInt() - 2 + ((Y.toInt() * size.toInt()) + (1 * size.toInt()))] <= 4 &&
                  matrix[X.toInt() - 2 + ((Y.toInt() * size.toInt()) + (1 * size.toInt()))] != 0) {
                targetX = X - 2;
                targetY = Y + 1;
              } else if (X + 2 < size &&
                  Y + 1 < size &&
                  Y + 1 < size &&
                  matrix[X.toInt() + 2 + ((Y.toInt() * size.toInt()) + (1 * size.toInt()))] >= 1 &&
                  matrix[X.toInt() + 2 + ((Y.toInt() * size.toInt()) + (1 * size.toInt()))] <= 4 &&
                  matrix[X.toInt() + 2 + ((Y.toInt() * size.toInt()) + (1 * size.toInt()))] != 0) {
                targetX = X + 2;
                targetY = Y + 1;
              } else if (X - 2 >= 0 &&
                  Y - 1 >= 0 &&
                  Y - 1 < size &&
                  matrix[X.toInt() - 2 + ((Y.toInt() * size.toInt()) - (1 * size.toInt()))] >= 1 &&
                  matrix[X.toInt() - 2 + ((Y.toInt() * size.toInt()) - (1 * size.toInt()))] <= 4 &&
                  matrix[X.toInt() - 2 + ((Y.toInt() * size.toInt()) + (1 * size.toInt()))] != 0) {
                targetX = X - 2;
                targetY = Y - 1;
              } else if (X + 2 < size &&
                  Y - 1 >= 0 &&
                  matrix[X.toInt() + 2 + ((Y.toInt() * size.toInt()) - (1 * size.toInt()))] >= 1 &&
                  matrix[X.toInt() + 2 + ((Y.toInt() * size.toInt()) - (1 * size.toInt()))] <= 4 &&
                  matrix[X.toInt() + 2 + ((Y.toInt() * size.toInt()) - (1 * size.toInt()))] != 0) {
                targetX = X + 2;
                targetY = Y - 1;
              } else {
                timer.cancel();
                matrix[X.toInt() + (Y.toInt() * size.toInt())] = 0;
                bool victory = true;
                matrix.forEach((item) {
                  if (item != 0) victory = false;
                  if (victory == true)
                    print("Конь закончил прогулку!");
                  else
                    print("Конь заблудился! ;(");
                });
              }
            }
          }

          matrix[X.toInt() + (Y.toInt() * size.toInt())] = 0;
        } else if (targetX.toInt() < X || targetX.toInt() > X + 0.01 * size * 1) {
          if (X < targetX) {
            dx = dx + 2 * speed;
          } else {
            dx = dx - 2 * speed;
          }
        } else if (targetY < Y - 0.01 * size * speed || targetY > Y) {
          if (Y < targetY)
            dy = dy + 2 * speed;
          else
            dy = dy - 2 * speed;
        } else {
          Provider.of<HorseSetting>(context).setblockStop(false);

          dx = targetX * Provider.of<HorseSetting>(context).sizeField;
          dy = targetY * Provider.of<HorseSetting>(context).sizeField;
          targetX = -1;
          targetY = -1;
        }
        if (dx < 0) {
          dx = 0;
        }
      });
    });
  }

  initMatrix() {
    int size = HorseSetting.size;
    for (int i = 0; i < size; i++) {
      for (int j = 0; j < size; j++) {
        switch (i % 4) {
          case 0:
            switch (j % 4) {
              case 0:
                matrix.add(1);
                break;
              case 1:
                matrix.add(2);
                break;
              case 2:
                matrix.add(3);
                break;
              case 3:
                matrix.add(4);
                break;
              default:
                break;
            }
            break;
          case 1:
            switch (j % 4) {
              case 0:
                matrix.add(3);
                break;
              case 1:
                matrix.add(4);
                break;
              case 2:
                matrix.add(1);
                break;
              case 3:
                matrix.add(2);
                break;
              default:
                break;
            }
            break;
          case 2:
            switch (j % 4) {
              case 0:
                matrix.add(2);
                break;
              case 1:
                matrix.add(1);
                break;
              case 2:
                matrix.add(4);
                break;
              case 3:
                matrix.add(3);
                break;
              default:
                break;
            }
            break;
          case 3:
            switch (j % 4) {
              case 0:
                matrix.add(4);
                break;
              case 1:
                matrix.add(3);
                break;
              case 2:
                matrix.add(2);
                break;
              case 3:
                matrix.add(1);
                break;
              default:
                break;
            }
            break;
        }
      }
    }
    int num = (size / 4).toInt();
    for (int i = num; i < size - num; i++) {
      for (int j = num; j < size - num; j++) {
        switch ((i - num) % 4) {
          case 0:
            switch (j % 4) {
              case 0:
                matrix[j + (i * size)] = 5;
                break;
              case 1:
                matrix[j + (i * size)] = 6;
                break;
              case 2:
                matrix[j + (i * size)] = 7;
                break;
              case 3:
                matrix[j + (i * size)] = 8;
                break;
              default:
                break;
            }
            break;
          case 1:
            switch (j % 4) {
              case 0:
                matrix[j + (i * size)] = 7;
                break;
              case 1:
                matrix[j + (i * size)] = 8;
                break;
              case 2:
                matrix[j + (i * size)] = 5;
                break;
              case 3:
                matrix[j + (i * size)] = 6;
                break;
              default:
                break;
            }
            break;
          case 2:
            switch (j % 4) {
              case 0:
                matrix[j + (i * size)] = 6;
                break;
              case 1:
                matrix[j + (i * size)] = 5;
                break;
              case 2:
                matrix[j + (i * size)] = 8;
                break;
              case 3:
                matrix[j + (i * size)] = 7;
                break;
              default:
                break;
            }
            break;
          case 3:
            switch (j % 4) {
              case 0:
                matrix[j + (i * size)] = 8;
                break;
              case 1:
                matrix[j + (i * size)] = 7;
                break;
              case 2:
                matrix[j + (i * size)] = 6;
                break;
              case 3:
                matrix[j + (i * size)] = 5;
                break;
              default:
                break;
            }
            break;
        }
      }
    }
  }

  Container containerHorse;


  double sizePhone;
  int size = HorseSetting.size;
  initWarp() {
    print(matrix);
    setState(() {
      wrap.children.clear();
    });
    sizePhone = widget.size;
    int color = 0;
    int line = 0;
    for (int i = 0; i < size; i++) {
      for (int j = 0; j < size; j++) {
        Container container = new Container(
          child: Center(child: Text(matrix[(i*size).toInt() + j.toInt()].toString())),
          width: (sizePhone / size),
          height: (sizePhone / size),
          color: matrix.isEmpty
              ? line % 2 == 0
                  ? color % 2 == 0 ? Colors.orangeAccent : Colors.deepOrangeAccent
                  : color % 2 == 0 ? Colors.deepOrangeAccent : Colors.orangeAccent
              : matrix[(i*size).toInt() + j.toInt()] == 0
                  ? Colors.grey
                  : line % 2 == 0
                      ? color % 2 == 0 ? Colors.orangeAccent : Colors.deepOrangeAccent
                      : color % 2 == 0 ? Colors.deepOrangeAccent : Colors.orangeAccent,
        );
        ++color;
        if (color == size) {
          ++line;
          color = 0;
        }
        setState(() {
          wrap.children.add(container);
        });
      }
      setState(() {

      });
    }
  }

  @override
  void initState() {
    super.initState();
    initMatrix();
    initWarp();

  }

  @override
  Widget build(BuildContext context) {
    Provider.of<HorseSetting>(context).sizeField = sizePhone / size;
    containerHorse = Container(
      child: InkWell(
        onTap: startTimeout,
        child: Image.network(
          'https://image.flaticon.com/icons/png/512/44/44967.png',
          height: Provider.of<HorseSetting>(context).sizeField,
        ),
      ),
      margin: EdgeInsets.only(
        left: dx,
        top: dy,
        right: 0.0,
        bottom: 0.0,
      ),
    );
    return Stack(
      children: <Widget>[
        wrap,
        containerHorse,
      ],
    );
  }
}
