import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:horse_run/providers/horse_settings.dart';
import 'package:provider/provider.dart';
import 'application/application.dart';

void main()=> runApp(MultiProvider(
  providers: [
    ChangeNotifierProvider(
      builder: (ctx) => HorseSetting(),
    ),
  ],
  child: Application(),
));